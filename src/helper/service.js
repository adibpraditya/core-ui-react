export function service(method, uri, body, user) {
  let BaseURL = "http://localhost:8000";
  return new Promise((resolve, reject) => {

    // Method POST ==================================================================================================================
    if (method === 'POST') {
      fetch(BaseURL + uri, {
        method: 'POST',
        headers: new Headers({
        }),
        body: JSON.stringify(body),
      })
        .then((response) => response.json())
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);

        });
    }
    //==================================================================================================================

    // Method GET ==================================================================================================================
    if (method === 'GET') {
      fetch(BaseURL + uri, {
        method: 'GET',
        headers: new Headers({
          'Authorization': 'Basic ' + btoa('mperso:u4VLBJbJKhdPx9sr'),
          'X_USERNAME': user,
          'Strict-Transport-Security': 'max-age=31536000',
        })
      })
        .then((response) => response.json())
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);

        });
    };
    //==================================================================================================================

    // Method DELETE ==================================================================================================================
    if (method === 'DELETE') {
      fetch(BaseURL + uri, {
        method: 'DELETE',
        headers: new Headers({
          'Authorization': 'Basic ' + btoa('mperso:u4VLBJbJKhdPx9sr'),
          'X_USERNAME': user,
          'Strict-Transport-Security': 'max-age=31536000',
        })
      })
        .then((response) => response.json())
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);

        });
    };
    //==================================================================================================================

  });
}
