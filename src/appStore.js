import { createStore } from 'redux'

const globalState = {
  login: false
}

const rootReducer = (state = globalState, action) => {
	if(action.type === 'LOGIN_SUCCESS'){
		console.log(action)
		return{
			...state,
			login: true,
			name: action.payload.data.name
		}
	}
	if(action.type === 'LOGOUT_SUCCESS'){
		return{
			...state,
			login: false
		}
	}
	return state;
}

const appStore = createStore(rootReducer)
export default appStore