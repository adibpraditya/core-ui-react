import React from 'react'
import { Link, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { service } from '../../../helper/service';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isValidUser: false,
      isLoaded: false,
      responseDesc: '',
      visible: false,
    };
  }

  login = () => {
      service('POST', '/user/login', "00219370", "Maumasuk123").then(async (result) => {
        //console.log(result);

        if(result.status){
            //dispatch({type: 'LOGIN_SUCCESS', payload: result})
          this.props.loginSucces(result)
        }
        /*
        var code = result.response_code;
        if (code === '00') {
          
          await this.setLocalStorage(result);
          this.setState({
            isValidUser: true,
          });
          
        }
        else {
          
          this.setState({
            isLoaded: false,
            responseDesc: result.response_desc,
            visible: true,
          });
          
        }
        */
      }).catch((error) => {
        console.log(error)
        /*
        this.setState({
          responseDesc: "Failed to Fetch API",
          visible: true,
        });
        */
      });
    //console.log("a");
    //this.props.loginSucces(); // this dispatches your action.
  }

  render() {
    if (this.props.login)
      return (<Redirect to={'/home'} />)
    
    return(
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="5">
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm>
                    <h1>Login</h1>
                    <p className="text-muted">Sign In to your account</p>
                    <CInputGroup className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput type="text" placeholder="Username" autoComplete="username" />
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput type="password" placeholder="Password" autoComplete="current-password" />
                    </CInputGroup>
                    <CRow>
                      <CCol xs="6">
                        <CButton color="primary" className="px-4" onClick={this.login}>Login</CButton>
                      </CCol>
                      <CCol xs="6" className="text-right">
                        <CButton color="link" className="px-0">Forgot password?</CButton>
                      </CCol>
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
    );
  };
}

const mapStateToProps = (state) => {
  return{
    login: state.login
  }
}

const mapDisptachToProps = (dispatch) => {
  return{
    loginSucces: (result) => dispatch({type: 'LOGIN_SUCCESS', payload: result}),
    logoutSucces: () => dispatch({type: 'LOGOUT_SUCCESS'})
  }
}

export default connect(mapStateToProps,mapDisptachToProps)(Login);
